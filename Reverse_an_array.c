#include <stdio.h>
int main()
{
    long long int n, i, j;
    scanf("%lld", &n);
    long long int arr[n], brr[n];
    for (i = 0; i < n; i++)
        scanf("%lld", &arr[i]);

    for (i = n - 1, j = 0; i >= 0; i--, j++)
    {
        brr[j] = arr[i];
    }

    for (i = 0; i < n; i++)
    {
        printf("%lld ", brr[i]);
    }
    printf("\n");
}
