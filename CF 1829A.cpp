// Link: https://codeforces.com/contest/1829/problem/A

#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve()
{
    int counter = 0, i = 0, j = 0;
    string main_s = "codeforces", str;
    cin >> str;

    while(j < str.size())
    {
        if(main_s[i] != str[j])
        {
            counter++;
        }

        i++;
        j++;
    }

    printf("%d\n", counter);
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc;
    cin >> tc;
    while(tc--)
    {
        solve();
    }
}
