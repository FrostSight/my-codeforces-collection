#include <stdio.h>
int main()
{
    int n, first, last, i, sum = 0;
    scanf("%d", &n);
    int arr[n];
    for (int i = 0; i < n; i++)
        scanf("%d", &arr[i]);

    scanf("%d%d", &first, &last);

    for (int i = first; i <= last; i++)
    {
        sum += arr[i];
    }
    printf("%d\n", sum);
}
