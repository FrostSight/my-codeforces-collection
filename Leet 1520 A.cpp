// Link: https://codeforces.com/problemset/problem/1520/A

#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve()
{
    //input part
    int n;
    cin >> n;

    vector<char> s(n);
    for(int i = 0; i < n; i++)
    {
        cin >> s[i];
    }

    //logic part
    unordered_set<char> seen;

    char last = s[0];
    seen.insert(last);

    for (int i = 1; i < n; i++)
    {
        if (s[i] != last)
        {
            if (seen.find(s[i]) != seen.end())
            {
                cout << "NO" << endl;
                return;
            }

            seen.insert(s[i]);
            last = s[i];
        }
    }

    cout << "YES" << endl;
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    int tc;
    cin >> tc;
    while (tc--)
    {
        // cout << "Case #" << t << ": ";
        solve();
    }
}
