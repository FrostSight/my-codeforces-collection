// Link: https://codeforces.com/contest/1519/problem/A

#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve()
{
    // input
    ll red, blue, diff;
    cin >> red >> blue >> diff;

    // logic

    if(red > blue)
        swap(red, blue); // always considering red to be minimum

    if(red * (diff + 1ll) < blue)
        cout << "NO" << endl;
    else
        cout << "YES" << endl;
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc;
    cin >> tc;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
