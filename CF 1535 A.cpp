// Link: https://codeforces.com/problemset/problem/1535/A

#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve()
{
    int num1, num2, num3, num4, max1, max2, min1, min2;

    cin >> num1 >> num2 >> num3 >> num4;

    max1 = max(num1, num2);
    max2 = max(num3, num4);
    min1 = min(num1, num2);
    min2 = min(num3, num4);

    if(max2 > max1 && max1 > min2)
        cout << "YES" << endl;
    else if(max1 > max2 && max2 > min1)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);

    int tc;
    cin >> tc;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
