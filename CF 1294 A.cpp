// Link: https://codeforces.com/contest/1294/problem/A

#include<iostream>
#include<stdio.h>
using namespace std;
int main()
{
    int a, b, c, n, sum, mex, t;

    scanf("%d",&t);
    while(t--)
    {
        scanf("%d%d%d%d", &a, &b, &c, &n);

        mex = max(a, max(b,c));
        sum = a+b+c+n;

        if(sum %3 ==0 && sum/3>=mex)
                printf("YES\n");
        else
                printf("NO\n");
    }
    return 0;
}

