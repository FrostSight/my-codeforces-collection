// Link: https://codeforces.com/problemset/problem/1536/A

#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve()
{
    int n;
    cin >> n;
    vector<int> arr(n);
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    int negatives = 0;
    vector<int> ans;

    for(int i = 0; i < n; i++)
    {
        if(arr[i] < 0)
        {
            negatives++;
            break;
        }
    }

    if(negatives > 0)
        cout << "NO" << endl;
    else
    {
        int maxVal = *max_element(arr.begin(), arr.end());
        cout << "YES" << endl << maxVal+1 << endl;
        for(int i = 0; i <= maxVal; i++)
            cout << i << " ";
        cout << endl;
    }

}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc;
    cin >> tc;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
