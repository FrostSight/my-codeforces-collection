#include <bits/stdc++.h>
using namespace std;

string converter(string s)
{
    for (int i = 0; i < s.size(); i++)
    {
        if(s[i] < 92)
            s[i] = s[i] + 32;
    }
    return s;
}

int main()
{
    string s1, s2, s3, s4, s5, s6;
    getline(cin, s1);
    getline(cin, s2);

    s1 = converter(s1);
    s2 = converter(s2);

    if(s1 > s2)
        printf("1");
    else if(s1 < s2)
        printf("-1");
    else
        printf("0");
}
