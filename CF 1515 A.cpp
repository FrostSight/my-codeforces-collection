// Link: https://codeforces.com/problemset/problem/1515/A

#include <bits/stdc++.h>
using namespace std;

#define arr array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;

void showOutput(vi& ans)
{
    for (int i = 0; i < ans.size(); i++)
    {
        cout << ans[i] << " ";
    }
    cout << endl;
}

void solve()
{
    // input part
    int n, defectWeight;
    cin >> n >> defectWeight;

    vi arr(n);
    for(int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    // logic part
    int totalSum = accumulate(arr.begin(), arr.end(), 0);
    if(totalSum == defectWeight)
    {
        cout << "NO" << endl;
        return;
    }

    sort(arr.begin(), arr.end());

    int sums = 0;
    for(int i = 0; i < n; i++)
    {
        sums += arr[i];
        if(sums == defectWeight)
        {
            if(i == n - 1)
            {
                cout << "NO" << endl;
                return;
            }
            swap(arr[i], arr[i + 1]);
            break;
        }
    }

    // output part
    cout << "YES" << endl;
    showOutput(arr);
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc;
    cin >> tc;
    while(tc--)
    {
        solve();
    }

}
