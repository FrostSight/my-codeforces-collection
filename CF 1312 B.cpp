// Link: https://codeforces.com/contest/1312/problem/B

#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int main()
{
    int a[100],n,i,t;

    scanf("%d",&t);

    while(t--)
    {
        scanf("%d",&n);

        for(i=0; i<n; i++)
            scanf("%d",&a[i]);

        sort(a, a+n);

        for(i=n-1; i>=0; i--)
                printf("%d ",a[i]);

        printf("\n");
    }

    return 0;
}

