// Link: https://codeforces.com/contest/1527/problem/A

#include <bits/stdc++.h>
using namespace std;
#define ll long long

void solve()
{
    ll n;
    cin >> n;
    int msb = log2(n);
    cout << (1 << msb) - 1 << endl;
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);

    int tc;
    cin >> tc;

    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}


/*
msb is 0, except msb all digits are 1
*/