// Link: https://codeforces.com/contest/1326/problem/B

#include<iostream>
using namespace std;
int main()
{
    long long int n,i;
    cin>>n;

    long long int b[n];
    for(i=0; i<n; i++)
    {
        cin>>b[i];
    }

    long long int x=0;

    for(i=0; i<n; i++)
    {
        b[i] = b[i] + x;
        cout<<b[i]<<" ";
        x = max(x, b[i]);
    }
    cout<<endl;
    return 0;
}

