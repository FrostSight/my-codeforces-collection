// Link: https://codeforces.com/contest/1325/problem/B

#include<bits/stdc++.h>
using namespace std;
int main()
{
    int i,t,n;

    scanf("%d",&t);
    while(t--)
    {
        scanf("%d",&n);
        set<int>value;

        for(i=0; i<n; i++)
        {
            int d;
            scanf("%d",&d);
            value.insert(d);
        }
        cout << value.size() << endl;
    }
    return 0;
}
 