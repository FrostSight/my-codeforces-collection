// Link: https://codeforces.com/contest/500/problem/A

#include <bits/stdc++.h>
using namespace std;

vector<int> graph[30000];
bool checked[30000];

void traversing(int startingNode)
{
    checked[startingNode] = true;
    for (int i = 0; i < graph[startingNode].size(); i++)
    {
        int adjacentNode = graph[startingNode][i];
        if (checked[adjacentNode] != true)
            traversing(adjacentNode);
    }
}

int main()
{
    int n, t;
    scanf("%d%d", &n, &t);
    for (int i = 1; i <= n - 1; i++)
    {
        int u; //second line input
        scanf("%d", &u);
        graph[i].push_back(i + u); //making the edge
    }

    traversing(1);

    if (checked[t] == 1)
        printf("YES\n");
    else
        printf("NO\n");
}
