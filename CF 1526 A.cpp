// Link: https://codeforces.com/contest/1526/problem/A

#include <bits/stdc++.h>
using namespace std;

#define arr array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void solve() 
{
    int n;
    cin >> n;

    n = n*2;

    ll arr[n+2];
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }
    
    // logic part

    int i = 0, j = n-1, counter = 0;
    sort(arr, arr+n);

    while(i <= j)
    {
        if(counter % 2 == 0)
        {
            cout << arr[j] << " ";
            j--;
        }
        else
        {
            cout << arr[i] << " ";
            i++;
        }

        counter++;
    }
}


int main() 
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc;
    cin >> tc;
    for (int t = 1; t <= tc; t++) 
    {
        solve();
    }
}

