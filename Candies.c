#include <stdio.h>
#include <math.h>
int main()
{
    long long int n, i, a, b, sum = 0;
    scanf("%lld", &n);
    long long int arr[n];
    for (i = 0; i < n; i++)
    {
        scanf("%lld", &arr[i]);
    }
    scanf("%lld%lld", &a, &b);

    i = a;
    while (i <= b)
    {
        sum = sum + arr[i];
        i++;
    }
    printf("%lld\n", sum);
}
